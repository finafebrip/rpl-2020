<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class TokoBangunanM extends CI_Model
{
    public function getToko()
    {
        $arr_toko[] = array(1,'Semen',10,5000);
        $arr_toko[] = array(2,'Batako',20,10000);
        $arr_toko[] = array(3,'Paku',50,31000);
        $arr_toko[] = array(4,'Sekrup',11,40000);
        $arr_toko[] = array(5,'Pasir',54,1000);
        $arr_toko[] = array(6,'Pipa',23,20000);
        $arr_toko[] = array(7,'Kawat',13,18000);
        $arr_toko[] = array(8,'Batu',51,11000);
        $arr_toko[] = array(9,'Bata',11,5000);
        $arr_toko[] = array(10,'Palu',25,9000);
        
        return $arr_toko;
    }
}